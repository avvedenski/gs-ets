package com.goldenspear.ets.communications;

public class EtsRequest {
	private String imsi;
	private String msisdn;
	private String imei;
	private String state;
	private String plmnId;
	private String tac;
	private String ratType;
	private String ratName;
	private String timeStamp;
	private String deviceMake;
	private String deviceModel;
	private String etcType;
	
	public boolean isValid() {
		return (imsi != null) || (msisdn != null);
	}
	
	/**
	 * @return the imsi
	 */
	public String getImsi() {
		return imsi;
	}
	/**
	 * @param imsi the imsi to set
	 */
	public void setImsi(String imsi) {
		this.imsi = imsi;
	}
	/**
	 * @return the msisdn
	 */
	public String getMsisdn() {
		return msisdn;
	}
	/**
	 * @param msisdn the msisdn to set
	 */
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
	/**
	 * @return the imei
	 */
	public String getImei() {
		return imei;
	}
	/**
	 * @param imei the imei to set
	 */
	public void setImei(String imei) {
		this.imei = imei;
	}
	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}
	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}
	/**
	 * @return the plmnId
	 */
	public String getPlmnId() {
		return plmnId;
	}
	/**
	 * @param plmnId the plmnId to set
	 */
	public void setPlmnId(String plmnId) {
		this.plmnId = plmnId;
	}
	/**
	 * @return the tac
	 */
	public String getTac() {
		return tac;
	}
	/**
	 * @param tac the tac to set
	 */
	public void setTac(String tac) {
		this.tac = tac;
	}
	/**
	 * @return the ratType
	 */
	public String getRatType() {
		return ratType;
	}
	/**
	 * @param ratType the ratType to set
	 */
	public void setRatType(String ratType) {
		this.ratType = ratType;
	}
	/**
	 * @return the ratName
	 */
	public String getRatName() {
		return ratName;
	}
	/**
	 * @param ratName the ratName to set
	 */
	public void setRatName(String ratName) {
		this.ratName = ratName;
	}
	/**
	 * @return the timeStamp
	 */
	public String getTimeStamp() {
		return timeStamp;
	}
	/**
	 * @param timeStamp the timeStamp to set
	 */
	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}
	/**
	 * @return the deviceMake
	 */
	public String getDeviceMake() {
		return deviceMake;
	}
	/**
	 * @param deviceMake the deviceMake to set
	 */
	public void setDeviceMake(String deviceMake) {
		this.deviceMake = deviceMake;
	}
	/**
	 * @return the deviceModel
	 */
	public String getDeviceModel() {
		return deviceModel;
	}
	/**
	 * @param deviceModel the deviceModel to set
	 */
	public void setDeviceModel(String deviceModel) {
		this.deviceModel = deviceModel;
	}
	/**
	 * @return the etcType
	 */
	public String getEtcType() {
		return etcType;
	}
	/**
	 * @param etcType the etcType to set
	 */
	public void setEtcType(String etcType) {
		this.etcType = etcType;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "EtsRequest [" + (imsi != null ? "imsi=" + imsi + ", " : "")
				+ (msisdn != null ? "msisdn=" + msisdn + ", " : "") + (imei != null ? "imei=" + imei + ", " : "")
				+ (state != null ? "state=" + state + ", " : "") + (plmnId != null ? "plmnId=" + plmnId + ", " : "")
				+ (tac != null ? "tac=" + tac + ", " : "") + (ratType != null ? "ratType=" + ratType + ", " : "")
				+ (ratName != null ? "ratName=" + ratName + ", " : "")
				+ (timeStamp != null ? "timeStamp=" + timeStamp + ", " : "")
				+ (deviceMake != null ? "deviceMake=" + deviceMake + ", " : "")
				+ (deviceModel != null ? "deviceModel=" + deviceModel + ", " : "")
				+ (etcType != null ? "etcType=" + etcType : "") + "]";
	}
	
	
}
	