package com.goldenspear.ets.communications;

import com.goldenspear.ets.model.User;

public class EtsResponse {
	public static enum Status {
		EXISTING,
		NEW,
		BAD_REQUEST
	}
	
	
	private String id;
	private Status status;
	
	public static final EtsResponse badRequest = new EtsResponse(Status.BAD_REQUEST);
	
	private EtsResponse(Status status) {
		this.status = status;
	}
	
	public EtsResponse(boolean existing, User user) {
		id = user.get_id();
		status = existing ? Status.EXISTING : Status.NEW;
	}
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the status
	 */
	public Status getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(Status status) {
		this.status = status;
	}
}
