package com.goldenspear.ets.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.goldenspear.ets.communications.EtsRequest;

import org.springframework.data.annotation.Id;
// import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;


// import java.util.Date;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Document(collection = "events")
public class Event {
    @Id
    private String _id;
    private String timestamp;
    private String userId;
	private String etcType;
	
	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}

	public Event(String id) {
		this._id = id;
	}

	public Event() {
	}

	public Event(EtsRequest request, User user) {
		timestamp = request.getTimeStamp();
		userId = user.get_id();
		etcType = request.getEtcType();
	}

	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @return the timestamp
	 */
	public String getTimestamp() {
		return timestamp;
	}

	/**
	 * @param timestamp the timestamp to set
	 */
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public String getEtcType() {
		return etcType;
	}

	public void setEtcType(String etcType) {
		this.etcType = etcType;
	}
}
