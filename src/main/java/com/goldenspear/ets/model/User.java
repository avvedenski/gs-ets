package com.goldenspear.ets.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.goldenspear.ets.communications.EtsRequest;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
//import org.springframework.stereotype.Indexed;


// import java.util.Date;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Document(collection = "users")
public class User {
    @Id
    private String _id;
    @Indexed
    private String globalId;
    @Indexed
	private String imsi;
    @Indexed
	private String msisdn;
    
	private String imei;
	private String plmnId;
	private String tac;
	private String deviceMake;
	private String deviceModel;
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "User [_id=" + _id + ", globalId=" + globalId + ", imsi=" + imsi + ", msisdn=" + msisdn + ", imei="
				+ imei + ", plmnId=" + plmnId + ", tac=" + tac + ", deviceMake=" + deviceMake + ", deviceModel="
				+ deviceModel + "]";
	}

	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}

	/**
	 * @return the globalId
	 */
	public String getGlobalId() {
		return globalId;
	}

	/**
	 * @param globalId the globalId to set
	 */
	public void setGlobalId(String globalId) {
		this.globalId = globalId;
	}

	/**
	 * @return the imsi
	 */
	public String getImsi() {
		return imsi;
	}

	/**
	 * @param imsi the imsi to set
	 */
	public void setImsi(String imsi) {
		this.imsi = imsi;
	}

	/**
	 * @return the msisdn
	 */
	public String getMsisdn() {
		return msisdn;
	}

	/**
	 * @param msisdn the msisdn to set
	 */
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	/**
	 * @return the imei
	 */
	public String getImei() {
		return imei;
	}

	/**
	 * @param imei the imei to set
	 */
	public void setImei(String imei) {
		this.imei = imei;
	}

	/**
	 * @return the plmnId
	 */
	public String getPlmnId() {
		return plmnId;
	}

	/**
	 * @param plmnId the plmnId to set
	 */
	public void setPlmnId(String plmnId) {
		this.plmnId = plmnId;
	}

	/**
	 * @return the tac
	 */
	public String getTac() {
		return tac;
	}

	/**
	 * @param tac the tac to set
	 */
	public void setTac(String tac) {
		this.tac = tac;
	}

	/**
	 * @return the deviceMake
	 */
	public String getDeviceMake() {
		return deviceMake;
	}

	/**
	 * @param deviceMake the deviceMake to set
	 */
	public void setDeviceMake(String deviceMake) {
		this.deviceMake = deviceMake;
	}

	/**
	 * @return the deviceModel
	 */
	public String getDeviceModel() {
		return deviceModel;
	}

	/**
	 * @param deviceModel the deviceModel to set
	 */
	public void setDeviceModel(String deviceModel) {
		this.deviceModel = deviceModel;
	}

	public User(String id) {
		this._id = id;
	}

	public User() {
	}

	public User(EtsRequest request) {
		imsi = request.getImsi();
		msisdn = request.getMsisdn();
		imei = request.getImei();
		plmnId = request.getPlmnId();
		tac = request.getTac();
		deviceMake = request.getDeviceMake();
		deviceModel = request.getDeviceModel();
	}
}
