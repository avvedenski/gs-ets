package com.goldenspear.ets.model;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserRepository extends MongoRepository<User, String> {
	
	User findByImsi(String imsi);
	User findByMsisdn(String msisdn);
	User findByImsiOrMsisdn(String imsi, String msisdn);
}
