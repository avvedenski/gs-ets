package com.goldenspear.ets.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Component;

import com.goldenspear.ets.model.Event;
import com.goldenspear.ets.model.EventRepository;

import org.springframework.beans.factory.annotation.Autowired;

@Component
public class EventDAO {
	@Autowired
	private EventRepository repository;

	public List<Event> list() {
		return repository.findAll();
	}

	public Event get(String id) {
		Optional<Event> result = repository.findById(id);
		return result.isPresent() ? result.get() : null;
	}

	public Event create(Event event) {
		repository.save(event);
	 return event;
	 }

	public void delete(String id) {
		repository.deleteById(id);
	}

	public Event update(Event event) {
		repository.save(event);
		return event;
	}

	public Event update(String id, Event event) {
		event.set_id(id);
		repository.save(event);
		return event;
	}
}
