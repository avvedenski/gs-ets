package com.goldenspear.ets.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Component;

import com.goldenspear.ets.model.User;
import com.goldenspear.ets.model.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;

@Component
public class UserDAO {
	@Autowired
	private UserRepository repository;

	public List<User> list() {
		return repository.findAll();
	}

	public User get(String id) {
		Optional<User> result = repository.findById(id);
		return result.isPresent() ? result.get() : null;
	}
	
	public User getByImsiOrMsisdn(String imsi, String msisdn) {
		return repository.findByImsiOrMsisdn(imsi, msisdn);
	}

	public User create(User user) {
		repository.save(user);
		return user;
	}

	public void delete(String id) {
		repository.deleteById(id);
	}

	public User update(User user) {
		repository.save(user);
		return user;
	}

	public User update(String id, User user) {
		user.set_id(id);
		repository.save(user);
		return user;
	}
}
