package com.goldenspear.ets.controller;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.goldenspear.utill.logging.GsLogger;
import com.goldenspear.utill.logging.GsLoggerFactory;

@Controller
public class ModelDate {
	
	final static GsLogger LOGGER = GsLoggerFactory.getLogger(ModelDate.class);
	
	ModelDate() {
//		LOGGER.info("ctr vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv.");
	}
	
	@RequestMapping(path={"/servlet-1"}, method=RequestMethod.GET)
	public String showTimestamp_S(Model model) {
	      model.addAttribute("message", "Hello Spring MVC! sevlet-1");
	     
	      DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.FULL).withZone(ZoneId.systemDefault());
	      LocalDateTime date = LocalDateTime.now();
	      
	      LOGGER.debug("vvvvvvvvv 1: " + date.toString());
	      LOGGER.debug("vvvvvvvvv 2: " + date.format(formatter));
	      
	      model.addAttribute("date", date.format(formatter));
	      
	      return "model";
	   }
	
	@RequestMapping(path={"/"}, method=RequestMethod.GET)
	public String showTimestamp0(Model model) {
	      model.addAttribute("message", "Hello Spring MVC! 0");
	     
	      DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.FULL).withZone(ZoneId.systemDefault());
	      LocalDateTime date = LocalDateTime.now();
	      
	      LOGGER.info("vvvvvvvvv 1: " + date.toString());
	      LOGGER.info("vvvvvvvvv 2: " + date.format(formatter));
	      
	      model.addAttribute("date", date.format(formatter));
	      
	      return "model";
	   }
	
	@RequestMapping(path={"/pts-1"}, method=RequestMethod.GET)
	public String showTimestamp(Model model) {
	      model.addAttribute("message", "Hello Spring MVC! 1");     
	      model.addAttribute("date", LocalDateTime.now());
	      
	      return "model";
	   }
	
	@RequestMapping(path={"/pts-2"}, method=RequestMethod.GET)
	public String showTimestamp2(Model model) {
	      model.addAttribute("message", "Hello Spring MVC! 2");	     
	      model.addAttribute("date", LocalDateTime.now());
	      
	      return "model";
	   }
}
