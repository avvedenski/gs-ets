package com.goldenspear.ets.controller;

//import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.DeleteMapping;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.goldenspear.ets.communications.EtsRequest;
import com.goldenspear.ets.communications.EtsResponse;
import com.goldenspear.ets.dao.EventDAO;
import com.goldenspear.ets.dao.UserDAO;
import com.goldenspear.ets.model.User;
import com.goldenspear.utill.logging.GsLogger;
import com.goldenspear.utill.logging.GsLoggerFactory;
import com.goldenspear.ets.model.Event;

@RestController
public class EventController {
	private static GsLogger LOGGER = GsLoggerFactory.getLogger(EventController.class);
	
	@Autowired
	private UserDAO userDAO;
	
	@Autowired
	private EventDAO eventDAO;

	@PostMapping(value = "/event")
	public ResponseEntity<EtsResponse> acceptEvent(@RequestBody EtsRequest request) {
		LOGGER.debug("vvv acceptEvent {} {}", System.identityHashCode(request), request);
		
		if (!request.isValid()) {
			return new ResponseEntity<EtsResponse>(EtsResponse.badRequest, HttpStatus.BAD_REQUEST);			
		}
		
		User user = userDAO.getByImsiOrMsisdn(request.getImsi(), request.getMsisdn());
		LOGGER.debug("vvv Found user {}", user);
		boolean existing = user != null;
		
		if(!existing) {
			user = new User(request);
			userDAO.create(user);
		}		
		
		LOGGER.debug("vvv user {} existing {}", (user == null) ? null : user.get_id(), existing);
		
		eventDAO.create(new Event(request, user));
		
		LOGGER.debug("vvv created new event");
		
		EtsResponse response = new EtsResponse(existing, user);

		return new ResponseEntity<EtsResponse>(response, HttpStatus.OK);
	}
}