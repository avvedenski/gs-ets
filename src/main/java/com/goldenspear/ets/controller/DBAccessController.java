package com.goldenspear.ets.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.PutMapping;
//import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.goldenspear.ets.dao.EventDAO;
import com.goldenspear.ets.dao.UserDAO;
import com.goldenspear.ets.model.User;
import com.goldenspear.ets.model.Event;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



@RestController
public class DBAccessController {
	private static Logger LOGGER = LoggerFactory.getLogger(DBAccessController.class);
	
	@Autowired
	private UserDAO userDAO;
	
	@Autowired
	private EventDAO eventDAO;


	@GetMapping("/users")
	public List<User> getUsers() {
		LOGGER.debug("getUsers");
		return userDAO.list();
	}

	@GetMapping("/events")
	public List<Event> getEvents() {
		LOGGER.debug("getEvents");
		return eventDAO.list();
	}
	
	/*
	
	@GetMapping("/customer/{id}")
	public ResponseEntity<User> getCustomer(@PathVariable("id") String id) {

		User user = userDAO.get(id);
		if (user == null) {
			return new ResponseEntity<User>(new User("No Customer found for ID " + id), HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<User>(user, HttpStatus.OK);
	}
	
	@PostMapping(value = "/customers")
	public ResponseEntity<User> createCustomer(@RequestBody User user) {

		userDAO.create(user);

		return new ResponseEntity<User>(user, HttpStatus.OK);
	}

	@DeleteMapping("/customer/{id}")
	public ResponseEntity<String> deleteCustomer(@PathVariable String id) {
		userDAO.delete(id);
		return new ResponseEntity<String>(id.toString(), HttpStatus.OK);

	}

	@PutMapping("/customer/{id}")
	public ResponseEntity<User> updateCustomer(@PathVariable String id, @RequestBody User user) {

		user = userDAO.update(id, user);

		if (null == user) {
			return new ResponseEntity<User>(new User("No Customer found for ID " + id), HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<User>(user, HttpStatus.OK);
	}
*/
}