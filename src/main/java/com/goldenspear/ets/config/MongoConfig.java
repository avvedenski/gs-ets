package com.goldenspear.ets.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import org.springframework.data.mongodb.core.MongoClientFactoryBean;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import com.mongodb.MongoClient;

/*
import static java.util.Collections.singletonList;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
*/

@Configuration
public class MongoConfig extends AbstractMongoConfiguration {

    /*
     * Factory bean that creates the com.mongodb.MongoClient instance
     */
     public @Bean MongoClientFactoryBean mongo() {
          MongoClientFactoryBean mongo = new MongoClientFactoryBean();
          mongo.setHost("localhost");
          return mongo;
     }

        @Override
        public String getDatabaseName() {
                return "vved_test";
        }


        @Override
        @Bean
        public MongoClient mongoClient() {
                return new MongoClient("127.0.0.1");
        }


/*
  @Override
  @Bean
  public MongoClient mongoClient() {
    return new MongoClient(singletonList(new ServerAddress("127.0.0.1", 27017)),
      singletonList(MongoCredential.createCredential("name", "db", "pwd".toCharArray())));
  }
*/

}
