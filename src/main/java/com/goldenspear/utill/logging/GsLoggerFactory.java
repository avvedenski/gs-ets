package com.goldenspear.utill.logging;

import org.slf4j.LoggerFactory;

public class GsLoggerFactory {

	public static GsLogger getLogger(Class<?> clazz) {
		
		return new GsLogger(LoggerFactory.getLogger(clazz));
	}

}
