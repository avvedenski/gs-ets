package com.goldenspear.utill.logging;

import org.slf4j.Logger;

public class GsLogger {
	private Logger logger;
	public GsLogger(Logger logger) {
		this.logger = logger;
	}

	public void debug(String format, Object arg) {
		logger.debug(format, arg);
	}

	public void debug(String format, Object ... arguments) {
		logger.debug(format, arguments);		
	}

	public void info(String msg) {
		logger.info(msg);				
	}
}
